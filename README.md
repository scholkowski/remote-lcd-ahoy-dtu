# Remote LCD Ahoy DTU
Remote display using the REST API of the Ahoy DTU and the Nokia 5110 display.\
Create "*credentials.h*" inside "*src/* " folder with the following content:

```
#ifndef CREDENTIALS_H
#define CREDENTIALS_H

// wifi login
const char *ssid = "<SSID>";
const char *password = "<PASS>";

// api url
const char *apiUrl = "http://<AHOY_DTU_IP>/api/live";

#endif
```

The PCB used (without the NRF24L01+ module) can be found at the official [Ahoy DTU Git](https://github.com/lumapu/ahoy/tree/main/tools/pcb-nokia5110).

![Display Front](resources/images/display_front.jpg)