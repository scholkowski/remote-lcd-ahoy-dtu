#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <U8g2lib.h>
#include <SPI.h>
#include <ArduinoJson.h>

#include "credentials.h"

// connection
WiFiClient wifiClient;
HTTPClient httpClient;

// define pins of display
#define CE 5
#define DC 4
#define RST 16

// use hardware spi
U8G2_PCD8544_84X48_1_4W_HW_SPI u8g2(U8G2_R0, CE, DC, RST);

void setup()
{
  Serial.begin(115200);

  WiFi.begin(ssid, password);

  u8g2.begin();
}

void loop()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    httpClient.begin(wifiClient, apiUrl);
    httpClient.GET();

    DynamicJsonDocument jsonDoc(2048);
    deserializeJson(jsonDoc, httpClient.getStream());

    auto currentPower = jsonDoc["inverter"][0]["ch"][0][2].as<String>();
    auto currentPowerUnit = jsonDoc["ch0_fld_units"][2].as<String>();
    auto yieldDay = jsonDoc["inverter"][0]["ch"][0][7].as<String>();
    auto yieldDayUnit = jsonDoc["ch0_fld_units"][7].as<String>();
    auto yieldTotal = jsonDoc["inverter"][0]["ch"][0][6].as<String>();
    auto yieldTotalUnit = jsonDoc["ch0_fld_units"][6].as<String>();

    char lineOne[100];
    sprintf(lineOne, "%-12s %s", currentPower.c_str(), currentPowerUnit.c_str());

    char lineTwo[100];
    sprintf(lineTwo, "%-12s %s", yieldDay.c_str(), yieldDayUnit.c_str());

    char lineThree[100];
    sprintf(lineThree, "%-12s %s", yieldTotal.c_str(), yieldTotalUnit.c_str());

    // set display content
    u8g2.firstPage();
    do
    {
      /* all graphics commands have to appear within the loop body. */
      u8g2.setFont(u8g2_font_profont10_tf);
      u8g2.drawStr(0, 6, "LEISTUNG:");
      u8g2.drawStr(0, 14, lineOne);
      u8g2.drawStr(0, 22, "HEUTE:");
      u8g2.drawStr(0, 30, lineTwo);
      u8g2.drawStr(0, 38, "GESAMT:");
      u8g2.drawStr(0, 46, lineThree);
    } while (u8g2.nextPage());

    httpClient.end();
  }
  else
  {
    // set display content
    u8g2.firstPage();
    do
    {
      /* all graphics commands have to appear within the loop body. */
      u8g2.setFont(u8g2_font_helvB08_tf);
      u8g2.drawStr(0, 8, "Offline");
    } while (u8g2.nextPage());
  }

  delay(20000);
}